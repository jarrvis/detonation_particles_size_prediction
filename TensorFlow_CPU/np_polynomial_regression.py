import tensorflow as tf
import numpy
import matplotlib.pyplot as plt
import configparser
import dataformatter as df
import dataplotter as dp

config = configparser.ConfigParser()
config.read('props.ini')
target = config['train_data']['target']
features = list(config['train_data']['non_categorical_features'].split(','))

# Use dataformatter for fetching data, updating from gitlab or ftp, returning features and target as properly formatted
dataFormatter = df.DataFormatter()
# Update training data from gitlab
dataFormatter.update_from_gitlab()

dataPlotter = dp.DataPlotter(dataFormatter.get_features(), dataFormatter.get_non_categorical_features(), dataFormatter.get_target())
dataPlotter.plot_per_category(dataFormatter.get_data_by_category())


category_data_dict = dataFormatter.get_data_by_category()
for key, value in category_data_dict.items():
    for feature in features:
        # Training Data
        train_X = numpy.asarray(value[feature])
        print(train_X)
        train_Y = numpy.asarray(value[target])
        
        x_data_smooth = numpy.linspace(min(train_X), max(train_X), 1000)
        fig, ax = plt.subplots(1,1)

        spl = numpy.poly1d(numpy.polyfit(train_X, train_Y,5))
        y_data_smooth = spl(x_data_smooth)
        ax.plot(train_X,train_Y, 'r')
        ax.plot(x_data_smooth, y_data_smooth, 'b')

        plt.show()