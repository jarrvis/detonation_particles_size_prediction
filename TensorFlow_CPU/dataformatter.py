import numpy as np
import configparser
import requests
import ftplib

class DataFormatter:

    train_data = []

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('props.ini')
        self.train_data_filepath = self.config['train_file']['train_file_path']
        self.train_data_filename = self.config['train_file']['train_file_name']
        try:
            self.train_data = np.genfromtxt("../"+ self.train_data_filename, delimiter=';', names=True, dtype=None)
        except IOError:
            self.update_from_gitlab()


    def update_from_gitlab(self):
        r = requests.get(self.train_data_filepath)
        if r.status_code is requests.codes.ok:
            file = open("../" + self.train_data_filename ,'w+')
            file.write(r.text)
            file.close()
            self.train_data = np.genfromtxt("../" + self.train_data_filename, delimiter=';', names=True, dtype=None)

    def update_from_ftp(self):
        ftp_ip = self.config['ftp_conf']['ftp_ip']
        ftp_user = self.config['ftp_conf']['ftp_user']
        ftp_pass = self.config['ftp_conf']['ftp_pass']
        ftp_path = self.config['ftp_conf']['ftp_path']
        ftp = ftplib.FTP(ftp_ip) 
        ftp.login(ftp_user, ftp_user) 
        ftp.cwd(ftp_path)
        ftp.retrbinary("RETR " + self.train_data_filename, open("../"+self.train_data_filename, 'rw').write)
        ftp.quit()
        self.train_data = np.genfromtxt("../"+train_data_filename, delimiter=';', names=True, dtype=None)

    def get_features(self):
        labels = list(self.config['train_data']['features'].split(','))
        features = []
        for label in labels:
            features.append(list(self.train_data[label]))
        if len(features) > 0:
            return features
        return []
    
    def get_non_categorical_features(self):
        labels = list(self.config['train_data']['non_categorical_features'].split(','))
        features = []
        for label in labels:
            features.append(list(self.train_data[label]))
        if len(features) > 0:
            return features
        return []

    def get_target(self):
        target = self.config['train_data']['target']
        if len(self.train_data[target]) > 0:
            return self.train_data[target]
        return []

    def get_data_by_category(self,category="fuel"): #parametrization will be added
        features = list(self.config['train_data']['features'].split(','))
        if category in features:
            train_data_by_cat = dict()
            labels = list(self.config['train_data'][category+"s"].split(','))
            category_col = np.array(self.train_data[category],dtype=str)
            for label in labels:
                out = [i for i, v in enumerate(category_col) if label in v]
                train_data_by_cat[label] = self.train_data[out]
        return train_data_by_cat